/**
 * Sample web component.
 * It just displays a string.
 */
class Sample extends HTMLElement {

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.data = "placeholder";
  }
  /** Static property returning an array of attributes observed by the browser
   */
  static get observedAttributes() {
    return ['data'];
  }
  /**
   * This is called when an observed attribute is
   * defined in the HTML or changed using Javascript.
   */
  attributeChangedCallback(property, old, now) {
    if (old === now) return;  // don't bother to render if there is nothing new
    this[property] = now;
  }

  /**
   * Called when the element is connected to a DOM.
   * Runs any required rendering.
   */
  connectedCallback() {
    this.shadowRoot.innerHTML += `<p>${this.data}</p>`;
  }
}



export { Sample };
