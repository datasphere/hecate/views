export { BarChart } from './barchart.js';
export { Histogram } from './histogram.js';
export { LineChart } from './linechart.js';
export { ScatterPlot } from './scatterplot.js';

/**
 * This object is a wrapper for three arrays
 * @typedef {Object} Hera
 * @property {Array<Object>} items - Items list
 * @property {Array<Object>} entities - Entities list
 * @property {Array<Object>} properties - Properties list
 */
