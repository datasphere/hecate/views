import * as d3 from 'd3';


/**
 * BarChart web component.
 * @see https://en.wikipedia.org/wiki/Bar_chart
 */
class BarChart extends HTMLElement{

  constructor(){
    super();
    this.attachShadow({ mode: 'open' });
    this.hera =  {};
    this.settings = {};
  }

    /** Static property returning an array of attributes observed by the browser
     */
    static get observedAttributes() {
      return ['data-hera', 'data-settings'];
    }

  get items() { return this.hera.items || []; }
  get entities() { return this.hera.entities || []; }
  get properties() { return this.hera.properties || []; }

  get pidx() { return this.settings.x || 'x'; }
  get pidy() { return this.settings.y || 'y'; }
  get color() { return this.settings.color || '#7b4c98'; }

  /**
   * This is called when an observed attribute is
   * defined in the HTML or changed using Javascript.
   * @param {!string} attribute - Attribute name
   * @param old - Previous value for `attribute`
   * @param now - Current value for `attribute`
   */
  attributeChangedCallback(attribute, old, now) {
    if (old === now) return;  // don't bother if there is nothing new
    this[attribute] = now;
    // data-* attributes can only be strings, so we cannot deserialize JSON
    // in them ; so we use plain object fields to store deserialized values
    // we trim the prefix to get the field name, so 'data-x' becomes 'x'
    const fieldName = attribute.split('-').slice(1).join('-');
    this[fieldName] = JSON.parse(now);
    // note that this does not defeat the purpose of using data-* attributes
    // in the first place, as if a name clash should happen in the future,
    // we could edit this class with no impact on the users code
  }
    
    //js method called everytime the component is added to the dom
  connectedCallback(){
    var dataset = this.prepareData(this.hera, this.settings);
  
        //creation of scene 
        const margin = { top : 70, right : 30, bottom: 40, left : 80};
        const {sceneWidth, sceneHeight, barWidth, svg} = this.createScene(500, 500, this.items.length, margin);
        
        //addition of axises
        var {y_scale, x_scale} = this.createAxisScales(sceneHeight, sceneWidth, dataset);
        this.addAxises(x_scale, y_scale, sceneHeight, sceneWidth, margin, svg)

        this.displayChart(sceneHeight, barWidth, dataset, y_scale, x_scale, svg);

    }

    /**
     * Sets the shadow dom of the component, currently creates a single div 
     * @returns the element that was appended to the shadow dom
     */ 
  createShadowDom() {
    const e = document.createElement('div');
    this.shadowRoot.appendChild(e);
    return e;
  }

    /**
     * Prepare the data by giving it the correct format
     * @param {object} rawData - the data as it was passed , it's a js object type 
     * @param {object} rawSettings - the settings as it was passed, it's a js object type
     * @returns js object that contains the elements of metadata, each key representing a different pid
     */
    prepareData(rawData, rawSettings){
        var barValues = [];
        var barLabels = [];
        rawData.items.forEach(item => {
            item.metadata.forEach(meta => {
                if (meta.pid === this.pidy) {  // Y axis is height of bars
                    barValues.push(parseInt(meta.value));
                }
                if (meta.pid === this.pidx) {  // X axis is "label for each bar"
                    barLabels.push(meta.value);
                }
            });
        });
        const dataset = barLabels.map((label, index) => ({
            label: label,
            value: barValues[index]
        }));
        return dataset;
    }

    /**
     * creates a svg element that will contain the viewer 
     * @param {number} width - desired width for the svg
     * @param {number} height - desired height for the svg
     * @param {number} dataLength - number of elements in data
     * @param {object} margin - to leave space for the axises, a js object containing 4 margins
     * @returns the svg and its height, width and the width of each bars
     */
    createScene(width, height, dataLength, margin){

        const sceneWidth = width - margin.left - margin.right; //to set barwidth, the key scenewith in the return is not recognized yet 
        const sceneHeight = height - margin.top - margin.bottom; 
        const barWidth = sceneWidth/dataLength
        
        const divElem = this.createShadowDom();
        const container =  d3.select(divElem);
        
        //addition of container to the svg 
        const svg = container.append("svg")
            .attr("width", sceneWidth + margin.left + margin.right)
            .attr("height", sceneHeight + margin.top + margin.bottom)
            .append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`);
        
        return {
            sceneWidth : sceneWidth, 
            sceneHeight : sceneHeight, 
            barWidth : barWidth,
            svg : svg
        };   
    }

    /**
     * create the x and y linear scales
     * @param {number} sceneHeight 
     * @param {number} sceneWidth 
     * @param {object} dataset 
     * @returns y_scale and x_scales
     */
    createAxisScales(sceneHeight, sceneWidth, dataset){
        var y_scale = d3.scaleLinear()
            .range([sceneHeight,0])
            .domain([0,d3.max(dataset.map(function(d) { return d.value; }))]);
        
        var x_scale = d3.scaleBand()
            .range([ 0, sceneWidth ])
            .domain(dataset.map(function(d) { return d.label; }))
            .padding(0.2);
        
        return{
            y_scale : y_scale,
            x_scale : x_scale
        }
    }

    /**
     * add the left and bottom axises, with their Label
     * @param {map} x_scale - linear scale for the x axis 
     * @param {map} y_scale - linear scale for the y axis
     * @param {number} sceneHeight - the height of the svg
     * @param {number} sceneWidth - the width of the svg
     * @param {object} margins 
     * @param {object} dataset 
     * @param {tag} svg 
     */
    addAxises(x_scale, y_scale, sceneHeight, sceneWidth, margins, svg){
        var y_axis = d3.axisLeft(y_scale);
        var x_axis = d3.axisBottom(x_scale);
        const labels = this.getAxesLabels();

        svg.append("g")
            .call(y_axis)
            /*TO DO : allow the user to customize the appearance of the axis
            the line below removes the line of vertical axis*/
            //.call((g) => g.select(".domain").remove()) 
            .call((g) => g.append("text")
                .attr("x", -margins.left)
                .attr("y",-10)
                .attr("fill", "currentColor")
                .attr("text-anchor", "start")
                .text(labels.y));

        svg.append("g")
            .attr("transform", `translate(0,${sceneHeight})`)
            .call(x_axis)
            .call((g) => g.append("text")
            .attr("x", sceneWidth)
            .attr("y", margins.bottom - 4)
            .attr("fill", "currentColor")
            .attr("text-anchor", "end")
            .text(labels.x))
            .selectAll("text");
                //TO DO : this lines allows to rotate the text of the horizontal axis
                //.attr("transform", `translate(-10,0)rotate(-45)`) 

    }

    /**
     * display the bar and puts it in the svg
     * @param {number} sceneHeight - the height of the svg
     * @param {number} barWidth - the width of each bars
     * @param {object} dataset 
     * @param {map} y_scale 
     * @param {map} x_scale 
     * @param {*} svg 
     */
    displayChart(sceneHeight, barWidth, dataset, y_scale, x_scale, svg){
        svg.selectAll("rect")
            .data(dataset)
            .enter()
            .append("rect")
            .attr("y", function(d) {
                return y_scale(d.value); 
            })
            .attr("x", function(d) {
                return x_scale(d.label); 
            })
            .attr("height", function(d) { 
                return sceneHeight - y_scale(d.value); 
            })
            .attr("width", barWidth) //add bar distance in future?

            .attr("fill", this.color)
            .attr("stroke","black");
    }

  getAxesLabels() {
    const px = this.getProperty(this.pidx);
    const py = this.getProperty(this.pidy);
    return {
      x: px ? px.name : `Missing property '${this.pidx}'`,
      y: py ? py.name : `Missing property '${this.pidy}'`,
    };
  }

  getProperty(pid) {
    const found = this.properties.find((p) => p['@id'] === pid);
    return found ? found : null;
  }
}



export { BarChart };
