import * as d3 from 'd3';


/**
 * LineChart web component.
 * @see https://en.wikipedia.org/wiki/Line_chart
 */
class LineChart extends HTMLElement{

  constructor(){
    super();
    this.attachShadow({ mode: 'open' });
    this.hera =  {};
    this.settings = {};
  }


    /** Static property returning an array of attributes observed by the browser
     */
    static get observedAttributes() {
      return ['data-hera', 'data-settings'];
    }

  get items() { return this.hera.items || []; }
  get entities() { return this.hera.entities || []; }
  get properties() { return this.hera.properties || []; }

  get pidx() { return this.settings.x || 'x'; }
  get pidy() { return this.settings.y || 'y'; }
  // WARNING: settings.color should be a valid color, ie. #xxx or
  // #xxxxxx or one of the acceptable words for color (purple, blue, ...)
  // if this value is invalid, line will be drawn, but stroke color
  // will be transparent, and thus invisible on any background
  get color() { return this.settings.color || '#7b4c98'; }

  /**
   * This is called when an observed attribute is
   * defined in the HTML or changed using Javascript.
   * @param {!string} attribute - Attribute name
   * @param old - Previous value for `attribute`
   * @param now - Current value for `attribute`
   */
  attributeChangedCallback(attribute, old, now) {
    if (old === now) return;  // don't bother if there is nothing new
    this[attribute] = now;
    // data-* attributes can only be strings, so we cannot deserialize JSON
    // in them ; so we use plain object fields to store deserialized values
    // we trim the prefix to get the field name, so 'data-x' becomes 'x'
    const fieldName = attribute.split('-').slice(1).join('-');
    this[fieldName] = JSON.parse(now);
    // note that this does not defeat the purpose of using data-* attributes
    // in the first place, as if a name clash should happen in the future,
    // we could edit this class with no impact on the users code
  }

  connectedCallback(){
    var dataset = this.prepareData();
   
        const margin = { top : 70, right : 30, bottom: 40, left : 80};
        var {sceneWidth, sceneHeight, svg} = this.createscene(margin, 600, 1200);
        
        var {x_scale, y_scale} = this.createAxisScales(sceneHeight, sceneWidth, dataset)
        this.addAxises(sceneWidth, sceneHeight, margin, x_scale, y_scale, svg);

        this.displayChart(x_scale, y_scale, dataset, svg);
  }

    /**
     * Sets the shadow dom of the component, currently creates a single div 
     * @returns the element that was appended to the shadow dom
     */ 
  createShadowDom() {
    const e = document.createElement('div');
    this.shadowRoot.appendChild(e);
    return e;
  }

  /**
   * Prepare the data by giving it the correct format
   * Creates a javascript object that contains the elements of metadata, each key representing a different pid
   * @param {Hera} data - Hera-formatted data parameter
   * @param {Object} settings - Settings parameter
   * @returns {Object} JS object containing the elements of metadata,
   * x (resp. y) key contains the value for x (resp. y) axis
   */
    prepareData(){
        var yValues = [];
        var xValues = [];
        this.items.forEach(item => {
            item.metadata.forEach(meta => {
                if (meta.pid === this.pidy) {
                    yValues.push(parseInt(meta.value));
                }
               if (meta.pid === this.pidx){
                    xValues.push(parseInt(meta.value));
                }
            });
        });
        const dataset = xValues.map((x, index) => ({
            x: x,
            y: yValues[index]
        }));
       return dataset;
    }

  /**
   * Creates a svg element that will contain the viewer.
   * @param {Object} margin - Space for the axes
   * @param {number} width - Desired int width for the svg
   * @param {number} height - Desired int height for the svg
   * @returns The svg and its height, width and the width of each bars
   */
    createscene(margin, height, width){
        const lineWidth = width - margin.left - margin.right;
        const lineHeight = height - margin.top - margin.bottom;

        // create svg and append it
        const divElem = this.createShadowDom();
        const container = d3.select(divElem);

        const svg = container.append("svg")
            .attr("width", lineWidth + margin.left + margin.right)
            .attr("height", lineHeight + margin.top + margin.left)
            .append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`);
        return {
            sceneWidth : lineWidth,
            sceneHeight : lineHeight,
            svg : svg
        }
    }
    
    /**
     * creates and sets x and y linear scales
     * @param {*} sceneHeight 
     * @param {*} sceneWidth 
     * @param {*} dataset 
     * @return x and y linear scales
     */
    createAxisScales(sceneHeight, sceneWidth, dataset){
        //set x and y scales
        var x_scale = d3.scaleLinear()
            .range([0,sceneWidth])
            .domain([0,d3.max(dataset, d => d.x)]);
        var y_scale = d3.scaleLinear()
            .range([sceneHeight,0])
            .domain([0,d3.max(dataset, d => d.y)]);

        return{
            x_scale : x_scale,
            y_scale : y_scale 
        }
        
    }


  /**
   * add the left and bottom axises, with their Label
   * @param sceneWidth - Scene width
   * @param x_scale - Linear scale for the X axis
   * @param y_scale - Linear scale for the Y axis
   * @param {*} sceneHeight
   * @param {*} sceneWidth
   * @param {*} margin
   * @param {*} svg
   */
    addAxises(sceneWidth, sceneHeight, margin, x_scale, y_scale, svg)
    {
            //add axises
            var x_axis = d3.axisBottom(x_scale);
            var y_axis = d3.axisLeft(y_scale);
        const labels = this.getAxesLabels();
    
            //x axis
            svg.append("g")
                .attr("transform", `translate(0,${sceneHeight})`)
                .call(x_axis)
                .call((g) => g.append("text")
                .attr("x", sceneWidth)
                .attr("y", margin.bottom - 4)
                .attr("fill", "currentColor")
                .attr("text-anchor", "end")
                .text(labels.x));
    
    
            //y axis    
            svg.append("g")
                .call(y_axis)
                .call((g) => g.append("text")
                .attr("x", -margin.left)
                .attr("y",-10)
                .attr("fill", "currentColor")
                .attr("text-anchor", "start")
                .text(labels.y));

    }
    /**
     * display the line chart
     * @param {*} x_scale 
     * @param {*} y_scale 
     * @param {*} dataset 
     * @param {*} svg 
     */
    displayChart(x_scale, y_scale, dataset, svg){
            //draw lines
            const line = d3.line()
                .x(d => x_scale(d.x))
                .y(d => y_scale(d.y));
    
            //add line path to svg
            svg.append("path")
                .datum(dataset)
                .attr("fill", "none")
                .attr("stroke", this.color)
                .attr("stroke-width", 1)
                .attr("d", line);
    }

  getAxesLabels() {
    const px = this.getProperty(this.pidx);
    const py = this.getProperty(this.pidy);
    return {
      x: px ? px.name : `Missing property '${this.pidx}'`,
      y: py ? py.name : `Missing property '${this.pidy}'`,
    };
  }

  getProperty(pid) {
    const found = this.properties.find((p) => p['@id'] === pid);
    return found ? found : null;
  }

}
export { LineChart };
