import * as d3 from 'd3';


/**
 * Histogram web component.
 * @see https://en.wikipedia.org/wiki/Histogram
 */
class Histogram extends HTMLElement{

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.hera = {};
    this.settings = {};
  }
  /**
   * Static property returning the array of attributes
   * observed by the browser.
   * @returns {Array<string>} Attribute names
   */
  static get observedAttributes() {
    return ['data-hera', 'data-settings'];
  }

  get items() { return this.hera.items || []; }
  get entities() { return this.hera.entities || []; }
  get properties() { return this.hera.properties || []; }

  get pidx() { return this.settings.x || 'x'; }
  get pidy() { return this.settings.y || 'y'; }
  get color() { return this.settings.color || '#7b4c98'; }

  get padding() {
    return this.settings.padding
        || { top: 20, right: 20, bottom: 30, left: 40 };
  }

  /**
   * This is called when an observed attribute is
   * defined in the HTML or changed using Javascript.
   * @param {!string} attribute - Attribute name
   * @param old - Previous value for `attribute`
   * @param now - Current value for `attribute`
   */
  attributeChangedCallback(attribute, old, now) {
    if (old === now) return;  // don't bother if there is nothing new
    this[attribute] = now;
    // data-* attributes can only be strings, so we cannot deserialize JSON
    // in them ; so we use plain object fields to store deserialized values
    // we trim the prefix to get the field name, so 'data-x' becomes 'x'
    const fieldName = attribute.split('-').slice(1).join('-');
    this[fieldName] = JSON.parse(now);
    // note that this does not defeat the purpose of using data-* attributes
    // in the first place, as if a name clash should happen in the future,
    // we could edit this class with no impact on the users code
  }

  /**
   * Called when the element is connected to a DOM.
   * Runs any required rendering.
   */
  connectedCallback(){

    this.width = this.parentElement.clientWidth;
    this.height = this.parentElement.clientHeight;

    const container = document.createElement('div');
    this.shadowRoot.appendChild(container);

    // group items into "bins", so that scene creation is easier
    const bins = this.prepareData();
    // create viewport, ie. d3 "scale" functions for axes
    const { vx, vy } = this.createViewport(bins);
    // create a SVG container for the whole scene
    const scene = d3.select(container).append('svg')
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('viewBox', [0, 0, this.width, this.height])
            .attr('style', 'max-width: 100%; height: auto;')
      ;
    // create callback for visual feedback when hovering on each histogram bar
    const { mouseover, mouseleave } = this.createHoverCallbacks();
    // create histogram bars
    scene.append('g')
      .attr('fill', this.color)
      .selectAll()
      .data(bins)
        .join('rect')
          .attr('x', (d) => vx(d.x0) + 1)
          .attr('width', (d) => vx(d.x1) - vx(d.x0) - 1)
          .attr('y', (d) => vy(d.length))
          .attr('height', (d) => vy(0) - vy(d.length))
          .on('mouseover', mouseover)
          .on('mouseleave', mouseleave)
          .append('title')
            .text((bin) => this.getBinLabel(bin))
    ;
    // add X axis and Y axis to scene
    this.addAxes(scene, vx, vy);
  }

  /**
   * Prepare the data by organize so that scene creation is simpler.
   * For this, we need to turn quantitative values into consecutive,
   * non-overlapping intervals called "bins", which we can in turn use
   * to group values in nice histogram bars.
   * @return Bin generator
   * @see https://d3js.org/d3-array/bin
   */
  prepareData() {
    var barValues = [];
    var classValues = [];
    this.items.forEach(item => {
      item.metadata.forEach(meta => {
        if (meta.pid === this.pidx) {
          barValues.push(parseInt(meta.value));
        }
        if (meta.pid === this.pidy){
          classValues.push(meta.value);
        }
      });
    });
    const dataset = classValues.map((label, index) => ({
        class: label,
        value: barValues[index],
      }));
    // Now, "bin" the dataset:
    const bins = d3.bin()
        //.thresholds(40)
      // if thresholds are specified as a number, the domain will be uniformly
      // divided into approximately that many bins: so if .thresholds(40) is
      // called, the resulting Histogram will have _appoximately_ 40 bars;
      // if thresholds is not specified, the current threshold generator will
      // be used, and by default it implements Sturges’ formula
        .value((d) => d.value)(dataset);
    return bins;
  }

  /**
   * Creates callbacks for changing each bar color when hovering onto.
   */
  createHoverCallbacks(msg) {
    return {
      mouseover: function(d) {
        d3.select(this).style('opacity', .5)
      },
      mouseleave: function(d) {
        d3.select(this).style('opacity', 1)
      },
    };
  }

  /**
   * Creates two scale functions. vx (resp. vy) is used to convert x (resp. y)
   * to a number between 0 and the width (resp. height) of the viewer.
   * @param {Array<Object>} bins - Array containing bins
   * @returns {Viewport} This viewer current viewport
   */
  createViewport(bins){
    return {
      vx: d3.scaleLinear()
        .domain([bins[0].x0, bins[bins.length - 1].x1])
        .range([this.padding.left, this.width - this.padding.right]),
      vy: d3.scaleLinear()
        .domain([0, d3.max(bins, (d) => d.length)])
        .range([this.height - this.padding.bottom, this.padding.top]),
    };
  }

  getBinLabel(bin) {
    const py = this.getProperty(this.pidy);
    const name = py ? py.name : `Missing property '${this.pidy}'`;
    return `No. of ${name}: ${bin.length}`;
  }

  /**
   * Add X and Y axes to the SVG scene.
   */
  addAxes(svg, vx, vy) {
    const labels = this.getAxesLabels();
    // add X-axis and label
    svg.append('g')
      .attr('transform', `translate(0,${this.height - this.padding.bottom})`)
      .call(d3.axisBottom(vx).ticks(this.width / 80).tickSizeOuter(0))
      .call((g) => g.append('text')
        .attr('x', this.width)
        .attr('y', this.padding.bottom - 4)
        .attr('fill', 'currentColor')
        .attr('text-anchor', 'end')
        .text(labels.x)
      );
    // add Y-axis and label
    svg.append('g')
      .attr('transform', `translate(${this.padding.left},0)`)
      .call(d3.axisLeft(vy).ticks(this.height / 40))
      .call((g) => g.append('text')
        .attr('x', -this.padding.left)
        .attr('y', 10)
        .attr('fill', 'currentColor')
        .attr('text-anchor', 'start')
        .text(`Frequency (#${labels.y})`)
      );
  }

  getAxesLabels() {
    const px = this.getProperty(this.pidx);
    const py = this.getProperty(this.pidy);
    return {
      x: px ? px.name : `Missing property '${this.pidx}'`,
      y: py ? py.name : `Missing property '${this.pidy}'`,
    };
  }

  getProperty(pid) {
    const found = this.properties.find((p) => p['@id'] === pid);
    return found ? found : null;
  }
}



export { Histogram };
