import * as d3 from 'd3';


/**
 * Scatterplot web component.
 * @see https://en.wikipedia.org/wiki/Scatter_plot
 * @see https://scottmurray.org/tutorials/d3/
 */
class ScatterPlot extends HTMLElement {

  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.hera = {};
    this.settings = {};
  }
  /**
   * Static property returning the array of attributes
   * observed by the browser.
   * @returns {Array<string>} Attribute names
   */
  static get observedAttributes() {
    return ['data-hera', 'data-settings'];
  }

  get items() { return this.hera.items || []; }
  get entities() { return this.hera.entities || []; }
  get properties() { return this.hera.properties || []; }

  get pidx() { return this.settings.x || 'x'; }
  get pidy() { return this.settings.y || 'y'; }
  get label() { return this.settings.label || 'label'; }
  get color() { return this.settings.color || '#7b4c98'; }

  get padding() {
    return this.settings.padding
        || { top: 3, right: 0, bottom: 10, left: 30 };
  }

  /**
   * This is called when an observed attribute is
   * defined in the HTML or changed using Javascript.
   * @param {!string} attribute - Attribute name
   * @param old - Previous value for `attribute`
   * @param now - Current value for `attribute`
   */
  attributeChangedCallback(attribute, old, now) {
    if (old === now) return;  // don't bother if there is nothing new
    this[attribute] = now;
    // data-* attributes can only be strings, so we cannot deserialize JSON
    // in them ; so we use plain object fields to store deserialized values
    // we trim the prefix to get the field name, so 'data-x' becomes 'x'
    const fieldName = attribute.split('-').slice(1).join('-');
    this[fieldName] = JSON.parse(now);
    // note that this does not defeat the purpose of using data-* attributes
    // in the first place, as if a name clash should happen in the future,
    // we could edit this class with no impact on the users code
  }

  /**
   * Called when the element is connected to a DOM.
   * Runs any required rendering.
   */
  connectedCallback() {
    this.width = this.parentElement.clientWidth;
    this.height = this.parentElement.clientHeight;
    const svgWidth = this.width + this.padding.right + this.padding.left;
    const svgHeight = this.height + this.padding.top + this.padding.bottom;

    const container = document.createElement('div');
    this.shadowRoot.appendChild(container);
    const svg = d3.select(container)
        .append('svg')
          .attr('viewBox', `0 0 ${svgWidth} ${svgHeight}`)
      ;
    const {vx, vy} = this.viewport;

    // create a dot for each item in this.items
    svg.selectAll('circle')
       .data(this.items)
         .enter()
         .append('circle')
           .attr('cx', (item) => vx(this.getItemX(item)))
           .attr('cy', (item) => vy(this.getItemY(item)))
           .attr('r',  (item) => this.getItemRadius(item))
           .attr('fill',  (item) => this.getItemColor(item))
           .append('title')
             .text((item) => this.getItemLabel(item))
      ;
    svg.selectAll('circle')
      .on('click', (event) => console.log(event));

    var xAxis = d3.axisBottom(vx).ticks(11);
    var yAxis = d3.axisLeft(vy).ticks(2);
    const labels = this.getAxesLabels();
    svg.append('g')
      .attr('transform', `translate(0,${this.height-this.padding.top})`)
      .call(xAxis)
      .call((g) => g.append('text')
        .attr('x', this.width)
        .attr('y', 0)
        .attr('fill', 'black')
        .attr('text-anchor', 'end')
        .text(labels.y))
      ;
    svg.append('g')
      .attr('transform', `translate(${this.padding.left},0)`)
      .call(yAxis)
      .call((g) => g.append('text')
        .attr('x', 0)
        .attr('y', '1em')
        .attr('fill', 'currentColor')
        .attr('text-anchor', 'start')
        .text(labels.x))
      ;
  }

  /**
   * @callback scale
   * @property {number} n - A number inside the scale's domain
   * @see https://d3js.org/d3-scale
   */

  /**
   * This object is a wrapper for two d3.scale objects, which can be used to
   * convert coordinates from the graph space to this viewer viewport space.
   * @typedef {Object} Viewport
   * @property {scale} vx - A scale returning the x (horizontal) coordinate
   * @property {scale} vy - A scale returning the y (vertical) coordinate
   * @see https://d3js.org/d3-scale
   */

  /**
   * Property returning this element viewport.
   * The viewport is composed of two scale functions, which can be used to
   * convert x (resp. y) to a number between 0 and the width (resp. height)
   * of the viewer.
   * @returns {Viewport} This viewer current viewport
   */
  get viewport() {
    const xMax = d3.max(this.items, (item) => this.getItemX(item));
    const yMax = d3.max(this.items, (item) => this.getItemY(item));
    // a scale is a function, domain is its input and range is its output
    const vx = d3.scaleLinear()
          .domain([0, xMax])
          .range([this.padding.left, this.width-this.padding.right]);
    const vy = d3.scaleLinear()
          .domain([0, yMax])
          .range([this.height-this.padding.top, this.padding.bottom]);
    // y is inverted because having the origin (0;0) of the scatter graph
    // in the bottom left corner feels more natural than the top left origin
    // which is standard in SVG.
    return { vx, vy };
  }

  getItemX(item) {
    return this.getItemMetadata(item, this.pidx);
  }
  getItemY(item) {
    return this.getItemMetadata(item, this.pidy);
  }
  getItemRadius(item) {
    return 5;
  }
  getItemColor(item) {
    return this.color;
  }
  getItemLabel(item) {
    const name = this.getItemMetadata(item, this.label);
    return name ? name : `Missing property '${this.label}'`;
  }
  getAxesLabels() {
    const px = this.getProperty(this.pidx);
    const py = this.getProperty(this.pidy);
    return {
      x: px ? px.name : `Missing property '${this.pidx}'`,
      y: py ? py.name : `Missing property '${this.pidy}'`,
    };
  }

  getItemMetadata(item, pid) {
    const found = item.metadata.find((m) => m.pid === pid);
    return found ? found.value : null;
  }
  getProperty(pid) {
    const found = this.properties.find((p) => p['@id'] === pid);
    return found ? found : null;
  }
}



export { ScatterPlot };
