import { Sample, } from './components/';
import { BarChart, Histogram, ScatterPlot, LineChart } from './views/';

customElements.define('sample-component', Sample);
customElements.define('scatter-plot', ScatterPlot);
customElements.define('bar-chart', BarChart);
customElements.define('histogram-chart', Histogram);
customElements.define('line-chart', LineChart);
