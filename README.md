# hecate-views

<a href='https://datasphere.readthedocs.io/projects/hecate/'><img src='https://sharedocs.huma-num.fr/wl/?id=zu51tqXtpv5e9lv8DXzu0Vrg9DLNKowD&fmode=download' width='100%'></a>

**Hecate** is a free web application for managing scientific databases.

**hecate-views** is a free Javascript library for visualizing scientific data.
Views are specific glances on subsets of your scientific database : lists, charts, maps, and so on.
Each view takes HERA-formatted data ([10.5281/zenodo.12795179](http://doi.org/10.5281/zenodo.12795179)) as its input, and can be easily configured to suit your needs.

## Resources

* [Documentation](https://datasphere.readthedocs.io/projects/hecate/)
* [Releases](https://www.npmjs.com/package/@cnrs/hecate-views?activeTab=versions)
* [Getting help](https://gitlab.huma-num.fr/datasphere/hecate/views/-/issues/)
