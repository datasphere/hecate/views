// @vitest-environment happy-dom
import { scaleLinear } from 'd3';
import { beforeAll, beforeEach, describe, it, expect, vi } from 'vitest';
import { ScatterPlot } from '/src/views';

const VIEWS = {
  ScatterPlot,
};

describe.each(
  Object.keys(VIEWS).map((name) => { return { classname: name }; })
)('$classname view', ({classname}) => {

  beforeAll(async () => {
    customElements.define('test-view', VIEWS[classname]);
  });

  beforeEach(async () => {
    this.viewer = new VIEWS[classname]();
  });

  it('has default padding settings', async () => {
    expect(this.viewer.padding).to.not.be.null;
    expect(this.viewer.padding.top).toBe(3);
    expect(this.viewer.padding.right).toBe(0);
    expect(this.viewer.padding.bottom).toBe(10);
    expect(this.viewer.padding.left).toBe(30);
  });

  it('can change padding settings', async () => {
    vi.spyOn(this.viewer, 'attributeChangedCallback')
    this.viewer.dataset.settings = '{"padding": {"top": 1, "right": -1, "bottom": 42, "left": 1312}}';
    expect(this.viewer.attributeChangedCallback).toHaveBeenCalledTimes(1);
    expect(this.viewer.padding).to.not.be.null;
    expect(this.viewer.padding.top).toBe(1);
    expect(this.viewer.padding.right).toBe(-1);
    expect(this.viewer.padding.bottom).toBe(42);
    expect(this.viewer.padding.left).toBe(1312);
    // shadow DOM is still not populated, however:
    expect(this.viewer.shadowRoot.childNodes).toHaveLength(0);
  });

  it('makes no new settings validation', async () => {
    vi.spyOn(this.viewer, 'attributeChangedCallback')
    this.viewer.dataset.settings = '{"padding": {"right": -1}}';
    expect(this.viewer.attributeChangedCallback).toHaveBeenCalledTimes(1);
    expect(this.viewer.padding).to.not.be.null;
    expect(this.viewer.padding.top).toBe(undefined);
    expect(this.viewer.padding.right).toBe(-1);
    expect(this.viewer.padding.bottom).toBe(undefined);
    expect(this.viewer.padding.left).toBe(undefined);
    // TODO: if this really happens, connectedCallback would probably break
  });

  it('has default viewport', async () => {
    const {vx, vy} = this.viewer.viewport;
    expect(vx).to.not.be.null;
    expect(typeof vx).toBe('function');
    expect(vy).to.not.be.null;
    expect(typeof vy).toBe('function');
    // we don't test further because scales behaviour are d3 specific
  });

});
