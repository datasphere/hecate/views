// @vitest-environment happy-dom
import { scaleLinear } from 'd3';
import { beforeAll, beforeEach, describe, it, expect, vi } from 'vitest';
import { BarChart, Histogram, LineChart, ScatterPlot } from '/src/views';

const VIEWS = {
  BarChart,
  Histogram,
  LineChart,
  ScatterPlot,
};

describe.each(
  Object.keys(VIEWS).map((name) => { return { classname: name }; })
)('$classname view', ({classname}) => {

  beforeAll(async () => {
    customElements.define('test-view', VIEWS[classname]);
  });

  beforeEach(async () => {
    this.viewer = new VIEWS[classname]();
  });

  it('has no items if none provided', async () => {
    expect(this.viewer.items).to.not.be.null;
    expect(this.viewer.items).toBeInstanceOf(Array);
    expect(this.viewer.items).toHaveLength(0);
  });

  it('has no entities if none provided', async () => {
    expect(this.viewer.entities).to.not.be.null;
    expect(this.viewer.entities).toBeInstanceOf(Array);
    expect(this.viewer.entities).toHaveLength(0);
  });

  it('has no properties if none provided', async () => {
    expect(this.viewer.properties).to.not.be.null;
    expect(this.viewer.properties).toBeInstanceOf(Array);
    expect(this.viewer.properties).toHaveLength(0);
  });

  it('does not populate shadow DOM with no input', async () => {
    expect(this.viewer.shadowRoot).to.not.be.null;
    expect(this.viewer.shadowRoot.childNodes).toHaveLength(0);
  });

  it('calls attributeChangedCallback on input via dataset', async () => {
    vi.spyOn(this.viewer, 'attributeChangedCallback')
    this.viewer.dataset.hera = '{"items":[{"metadata":[]}]}';
    expect(this.viewer.attributeChangedCallback).toHaveBeenCalledTimes(1);
    expect(this.viewer.items).toHaveLength(1);
    // shadow DOM is still not populated, however:
    expect(this.viewer.shadowRoot.childNodes).toHaveLength(0);
  });

  it('calls attributeChangedCallback on setAttribute', async () => {
    vi.spyOn(this.viewer, 'attributeChangedCallback')
    this.viewer.setAttribute('data-hera', '{"items":[{"metadata":[]}]}');
    expect(this.viewer.attributeChangedCallback).toHaveBeenCalledTimes(1);
    // shadow DOM is still not populated, however:
    expect(this.viewer.shadowRoot.childNodes).toHaveLength(0);
  });

  it('populates shadow DOM once element is added to DOM', async () => {
    vi.spyOn(this.viewer, 'connectedCallback')
    this.viewer.dataset.hera = '{"items":[{"metadata":[]}]}';
    expect(this.viewer.connectedCallback).toHaveBeenCalledTimes(0);
    expect(this.viewer.items).toHaveLength(1);
    // when we add viewer to the DOM
    document.body.appendChild(this.viewer);
    // then, shadow DOM is now populated:
    expect(this.viewer.connectedCallback).toHaveBeenCalledTimes(1);
    expect(this.viewer.shadowRoot.childNodes).toHaveLength(1);
    expect(this.viewer.items).toHaveLength(1);
  });

});
