// @vitest-environment happy-dom
import { describe, it, expect } from 'vitest';
import { Sample } from '/src/components/sample.js';

describe('Sample component', () => {
  it('is built with "placeholder" value', async () => {
    const e = new Sample();
    expect(e.data).to.not.be.null;
  });
});
